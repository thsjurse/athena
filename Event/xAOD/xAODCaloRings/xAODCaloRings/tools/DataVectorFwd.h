/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

// $Id: DataVectorFwd.h 767579 2016-08-11 13:57:39Z ssnyder $ 
#ifndef XAODCALORINGS_TOOLS_DATAVECTORFWD_H
#define XAODCALORINGS_TOOLS_DATAVECTORFWD_H

/// Forward declare DataVector:
template<class T, class BASE> class DataVector;

#endif // XAODCALORINGS_TOOLS_DATAVECTORFWD_H
